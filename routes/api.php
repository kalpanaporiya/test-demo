<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Apis;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::post('register', 'UserController@register');
Route::post('register/{token}', [Apis\UserController::class, 'register']);
Route::post('login', [Apis\UserController::class, 'authenticate']);
Route::post('verify-otp', [Apis\UserController::class, 'verify_otp'])->name('api.user.otp_verification');
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('/user', [Apis\UserController::class, 'getAuthenticatedUser']);
    Route::post('/update-profile', [Apis\UserController::class, 'update_user_profile']);
});
