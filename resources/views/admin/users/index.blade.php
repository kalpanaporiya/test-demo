@extends('admin.layouts.default')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/vendors/data-tables/css/jquery.dataTables.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin//vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/pages/page-users.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/vendors/dropify/css/dropify.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/vendors/select2/select2-materialize.css') }}" />
<div class="content-wrapper-before gradient-45deg-indigo-purple cls-dashboard"></div>
@include('admin.includes.breadcrumbs')
<style>
    select[name="dt-users-data_length"] {
        display: none !important;
    }
</style>
<div class="col s12">
    <div class="container">
        <!-- users list start -->
        <section class="users-list-wrapper section">
            <div class="card-panel">
                <div class="row right-align">
                    <div class="show-btn">
                        <button type="button" class="btn waves-effect waves-light btn modal-trigger btn-invite-new-user" data-target="mdl-invite-new-users">Invite New User</button>
                    </div>
                </div>
            </div>
            <div class="users-list-table">
                <div class="card">
                    <div class="card-content">
                        <form id="frm-data">
                            <table id="dt-users-data" class="table" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- Modal Structure -->
<div id="mdl-invite-new-users" class="modal">
    <div class="modal-content">
        <div class="row">
            <div class="col s12">
                <h6 class="card-title">New User Invitation</h6>
                <form class="formValidate" autocomplete="off" id="frm-invite-new-user" method="post" enctype="multipart/form-data" action="{{ route('admin.send.invitation') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="email">Email Address*</label>
                            <input id="email" name="email" autocomplete="off" type="text" data-error=".errorTxt1">
                            <small class="errorTxt1"></small>
                        </div>                        
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light right submit" type="submit" name="action">Save
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="mdl-edit-fields-data" class="modal">
    <div class="modal-content">
        <div class="row">
            <div class="col s12">
                <h4>Edit Race Data</h4>
                <form class="formValidate" autocomplete="autocomplete_off" id="frm-edit-race-data" method="post" enctype="multipart/form-data" action="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="row">
                        <input type="hidden" id="id" name="id" />
                        <div class="input-field col s12">
                            <input type="text" autocomplete="autocomplete_off" id="fields" autocomplete="off" placeholder="Fields" name="fields" />
                        </div>
                        <div class="input-field col s12">
                            <input type="number" id="race" autocomplete="off" placeholder="Race #" name="race" />
                        </div>
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light right submit" type="submit" name="action">Save
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('assets/admin/vendors/data-tables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/admin//vendors/dropify/js/dropify.min.js') }}"></script>
<script src="{{ asset('assets/admin/vendors/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/admin/vendors/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ asset('assets/admin/vendors/select2/select2.full.min.js') }}"></script>
<script>
    var routes = {
        'get_data': `{{ route('admin.get.users') }}`,
        'chk_email': `{{ route('admin.chk.email') }}`,
        'remove_data': ``,
        'empty_data': ``,
    }
</script>
<script src="{{ asset('js/users.js') }}"></script>
@stop