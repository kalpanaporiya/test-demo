$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });        
    $('.modal').modal();
});
if ($('#dt-users-data').length > 0) {
    var table = $("#dt-users-data").DataTable({
        processing: true,
        serverSide: true,
        serverMethod: 'post',
        searching: false,
        "language": {
            "processing": "Loading..."
        },
        ajax: {
            url: routes.get_data,
            data: function (d) {
                d._token = $('meta[name="csrf-token"]').attr('content');
                d.horse_name = $('#slct-horse-name').val();
            },
        },
        order: [
            [0, 'DESC']
        ],
        columns: [
            {
                data: "first_name",
            },
            { data: "last_name" },
            {
                data: "email",
                render: function (data, type) {
                    return '<span class="blue-text">' + data + '</span>';
                }
            },
            { data: "last_name" },
        ]
    });
}

$(document).on('click', '.btn-invite-new-user', function () {
    $('#mdl-invite-new-users').modal('open');
});
if ($('#frm-invite-new-user').length > 0) {
    $("#frm-invite-new-user").validate({
        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: routes.chk_email,
                    type: "GET",
                },
            },
        },
        errorElement: 'div',
        errorPlacement: function (error, element) {
            var placement = '.errorTxt1';
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        messages: {
            email: {
                required: "Please enter email address.",
                extension: "Please enter a valid email address.",
                remote: "This email addrss is already exist.",
            },
        },
        submitHandler: function (form) {
            $(".preloader-Box").show();
            $.ajax({
                url: $(form).attr('action'),
                method: "POST",
                async: true,
                data: new FormData($(form)[0]),
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response.status == true) {
                        toastr["success"](response.message);
                        $('#dt-users-data').DataTable().ajax.reload();
                    } else {
                        toastr["error"](response.message);
                        $('#dt-users-data').DataTable().ajax.reload();
                    }
                    clearUploadForm(form);
                }, error: function (err) {
                    clearUploadForm(form);
                    toastr["success"]("Invitation sent successfully!");
                    $('#dt-users-data').DataTable().ajax.reload();
                }
            });
            return false;
        }
    });
}

function clearUploadForm(form = '') {
    $('#mdl-invite-new-users').modal('close');    
    $(form).trigger("reset");    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


