<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserNameInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('user_name')->nullable()->default('')->after('last_name');
            $table->string('avatar')->nullable()->default('')->after('password');
            $table->enum('user_role',['admin', 'user'])->default('user')->nullable()->after('avatar');
            $table->string('otp_code')->nullable()->default('')->after('is_admin');
            $table->dateTime('registered_at')->nullable()->useCurrent()->after('otp_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['user_name', 'avatar', 'user_role', 'otp_code','registered_at']);
        });
    }
}
