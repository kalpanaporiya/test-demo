<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

function pr($string, $isnull = false) {
    echo '<pre>';
    print_r($string);
    echo '</pre>';
    if ($isnull)
        die;
}

/**
 * @name saveAdminErrorLog
 * @author Kalpana
 */
function saveAdminErrorLog($ErrorDetails = []) {
    $orderLog = new Logger('admin');
    $orderLog->pushHandler(new StreamHandler(storage_path('logs/admin.log')), Logger::INFO);
    $orderLog->info('AdminLog', $ErrorDetails);
}

function getControllerOrActionName($type = 0) {
    $currentAction = \Route::currentRouteAction();
    list($controller, $method) = explode('@', $currentAction);
    if (!empty($type)) {
        return preg_replace('/.*\\\/', '', $method);
    }
    return preg_replace('/.*\\\/', '', $controller);
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

