<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    /**
     * @name authenticate
     * @author Kalpana
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            $user = User::where(['email' => $request->get('email')])->whereNotNull('email_verified_at')->first();            
            if (!empty($user)) {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } else {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }
    /**
     * @name register
     * @author Kalpana
     */
    public function register(Request $request, $remember_token = '')
    {
        $validator = Validator::make($request->all(), [
            'user_name' => 'required|string|max:20|min:4',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = User::where(['remember_token' => $remember_token])->first();
        $otp_code = random_int(100000, 999999);
        if (!empty($user)) {
            User::where(['id' => $user->id])->update([
                'first_name' => $request->get('user_name'),
                'user_name' => $request->get('user_name'),
                'password' => Hash::make($request->get('password')),
                'otp_code' => $otp_code,
                'remember_token' => '',
            ]);
            $From = env('MAIL_FROM_ADDRESS');
            $FromName = env('MAIL_FROM_NAME');
            if (!empty($user->email) && !empty($From)) {
                $verification_url = env('APP_URL') . '/api/verify-otp';
                $data = ['to' => $user->email, 'subject' => 'Test Demo - Received OTP', 'from' => $From, 'fromname' => $FromName, 'otp_code' => $otp_code, 'first_name' => $request->get('user_name'), 'verification_url' => $verification_url];
                \Mail::send('admin.users.otp_verification', $data, function ($m) use ($data) {
                    $m->from($data['from'], $data['fromname']);
                    $m->to($data['to'], $data['to'])->subject($data['subject']);
                });
            }
            return response()->json(['Congratulation! Your first step completed Sucessfully. Please chek your email! We have sent OTP on "' . $user->email . '". '], 201);
        } else {
            return response()->json(['Invalid registration link!'], 404);
        }
    }
    /**
     * @name getAuthenticatedUser
     * @author Kalpana
     */
    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }

    /**
     * @name verify_otp
     * @author Kalpana
     */
    public function verify_otp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp_code' => 'required|digits:6',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        } else {
            $user = User::where(['otp_code' => $request->otp_code])->first();
            if (!empty($user)) {
                $user = User::where(['id' => $user->id])->update([
                    'otp_code' => '',
                    'email_verified_at' => date('Y-m-d H:i:s'),
                ]);
                $data['message'] = 'Congratulation! The email verification process done successfully!';
                return response()->json($data, 201);
            } else {
                return response()->json(['user_not_found'], 404);
            }
        }
    }

    /**
     * @name update_user_profile
     * @author Kalpana
     */
    public function update_user_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:20|min:4',
            'last_name' => 'required|string|max:20|min:4',
            'avtar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=256,height=256',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        } else {
            try {
                if (!$user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
                } else {
                    #--- Upload the avtar image on server
                    $imageName = time() . '.' . $request->avtar->extension();
                    $request->avtar->move(public_path('users_avtars'), $imageName);

                    #--- update the data
                    User::where(['id' => $user->id])->update([
                        'first_name' => $request->get('first_name'),
                        'last_name' => $request->get('last_name'),
                        'avatar' => asset('users_avtars/' . $imageName),
                    ]);
                    $user = JWTAuth::parseToken()->authenticate();
                    return response()->json(compact('user'), 201);
                }
            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                return response()->json(['token_expired'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                return response()->json(['token_invalid'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                return response()->json(['token_absent'], $e->getStatusCode());
            }
            return response()->json(compact('user'));
        }
    }
}
