<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use DataTables;
use Validator;
use Illuminate\Support\Str;

class UsersController extends Controller
{

    /**
     * @name index
     * @author Kalpana
     */
    public function index()
    {
        return view("admin.users.index");
    }

    /**
     * @name get_users
     * @author Kalpana
     */
    public function get_users()
    {
        return Datatables::collection(User::all())->make(true);
    }

    /**
     * To check email is unique or not
     * @param  Request $request
     *
     * @return json
     */
    public function check_register_email(Request $request)
    {
        $email = User::where('email', trim($request->email))->first();
        if (!$email) {
            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
    }

    /**
     * @name send_invitation
     * @author Kalpana
     */
    public function send_invitation(Request $request)
    {
        $data['status'] = false;
        $data['message'] = 'Something went wrong here!';
        if ($request->ajax()) {
            $validator = Validator::make($request->all(), ['email' => 'required|string|email|max:255|unique:users']);
            if ($validator->fails()) {
                $data['message'] = 'Please enter valid email!';
            } else {
                try {
                    $remember_token = Str::random(30);
                    $user = User::create([
                        'first_name' => '',
                        'email' => $request->email,
                        'password' => '',
                        'remember_token' => $remember_token,
                    ]);
                    if (!empty($user)) {
                        $From = env('MAIL_FROM_ADDRESS');
                        $FromName = env('MAIL_FROM_NAME');
                        $registration_url = env('APP_URL') . '/api/register/' . $remember_token;
                        if (!empty($request->email) && !empty($From)) {
                            $data = ['to' => $request->email, 'subject' => 'Test Demo - Invitation For Registration', 'from' => $From, 'fromname' => $FromName, 'registration_url' => $registration_url];
                            \Mail::send('admin.users.invitation_mail', $data, function ($m) use ($data) {
                                $m->from($data['from'], $data['fromname']);
                                $m->to($data['to'], $data['to'])->subject($data['subject']);
                            });
                        }
                        $data['status'] = true;
                        $data['message'] = 'Invitation sent successfully!';
                    }
                } catch (\Exception $ex) {
                    $ErrorDetails = ['message' => 'Error in send invitation', 'description' => $ex->getMessage()];
                    saveAdminErrorLog($ErrorDetails);                    
                    $data['message'] = 'Process stopped due to Error while sending user invitation!';
                }
            }
        }
        return response()->json($data);
    }
}
